import java.util.*;

abstract class base
{
    base()
    {
        System.out.println("this is a abstract class constructor:\n");
    }
    abstract void print();
    void wish()
    {
        System.out.println("hello everyone\n");
    }
}

class abs extends base
{
    void print()
    {
        System.out.println("java is easy\n");
    }
    public static void main(String[] args)
    {
    
        abs i=new abs();
        i.wish();
    
        i.print();
        //i.base();
    }

}
